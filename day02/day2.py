
with open('input') as input:
    input = input.readlines()

def part1():
    valid = 0
    for line in input:
        split = [s.strip() for s in line.split(':')]
        policy = split[0]
        prange = policy.split()[0]
        pchar = policy.split()[1].strip(':')
        pmin = int(prange.split('-')[0].strip())
        pmax = int(prange.split('-')[1].strip())
        passwd = split[1]

        pcount = passwd.count(pchar)

        if pcount >= pmin and pcount <= pmax:
            valid += 1

    print(valid)

def part2():
    valid = 0
    for line in input:
        split = [s.strip() for s in line.split(':')]
        policy = split[0]
        prange = policy.split()[0]
        pchar = policy.split()[1].strip(':')
        pmin = int(prange.split('-')[0].strip())
        pmax = int(prange.split('-')[1].strip())
        passwd = split[1]

        p1 = passwd[pmin - 1]
        p2 = passwd[pmax - 1]

        if (p1 == pchar or p2 == pchar) and p1 != p2:
            valid += 1

    print(valid)

part2()
