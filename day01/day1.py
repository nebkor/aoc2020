
with open('input') as input:
    input = input.readlines()

def part1():
    s = set()
    nums = [int(x) for x in input]
    for x in nums:
        d = 2020 - x
        if d in s:
            print("{} * {} = {}".format(
                x,
                d,
                x * d
            ))
        else:
            s.add(x)

def part2():
    s = set()
    nums = [int(x) for x in input]
    for x in nums:
        for c in s:
            d = 2020 - x - c
            if d in s:
                print("{} * {} * {} = {}".format(
                    x,
                    d,
                    c,
                    x * d * c
                ))
                return
        s.add(x)


part2()
