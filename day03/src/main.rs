fn main() {
    let rows: Vec<_> = include_str!("../input").trim().split('\n').collect();
    let num_cols = rows[0].len();

    let mut s11 = 0u64;
    let mut s31 = 0u64;
    let mut s51 = 0u64;
    let mut s71 = 0u64;
    let mut s12 = 0u64;

    for y in 0..rows.len() {
        for s in &[1, 3, 5, 7] {
            let x = s * y;
            let ix = x % num_cols;
            let col: Vec<_> = rows[y].chars().collect();
            let ch = col[ix];
            if ch == '#' {
                match s {
                    1 => s11 += 1,

                    3 => s31 += 1,
                    5 => s51 += 1,
                    7 => s71 += 1,
                    _ => unreachable!(),
                }
            }
            if y % 2 == 0 && *s == 1 {
                let x = y / 2;
                let ix = x % num_cols;
                if col[ix] == '#' {
                    s12 += 1;
                }
            }
        }
    }
    println!("part1: {}", s31);
    println!("part2: {}", s11 * s31 * s51 * s71 * s12);
}
